local storage = require("openmw.storage")
local self = require("openmw.self")
local types = require("openmw.types")
local ui = require("openmw.ui")
local I = require("openmw.interfaces")
local ambient = require('openmw.ambient')
local async = require("openmw.async")

local lastKnownLevel
local trainingsRemaingForLevel

local delay1 = 0.1
local delay2 = 0.1

local settings = storage.playerSection("SettingsAT")

local pendingTraining = false
local function checkForTraining()
    if pendingTraining then
        local soundPlaying = ambient.isSoundPlaying("skillraise")
        if soundPlaying then
            trainingsRemaingForLevel = trainingsRemaingForLevel - 1
        end
        pendingTraining = false
    end
end
return {
    engineHandlers = {
        onSave = function()
            return { lastKnownLevel = lastKnownLevel, trainingsRemaingForLevel = trainingsRemaingForLevel }
        end,
        onLoad = function(data)
            if not data then return end
            if not data.lastKnownLevel then return end
            trainingsRemaingForLevel = data.trainingsRemaingForLevel
            lastKnownLevel = data.lastKnownLevel
        end
    },

    eventHandlers = {
        UiModeChanged = function(data)
            -- print('LMMUiModeChanged to', data.newMode, '(' .. tostring(data.arg) .. ')')

            
            if data.newMode == "Training" then
                local level = types.Actor.stats.level(self).current
                if not lastKnownLevel then
                    lastKnownLevel = level
                end
                if trainingsRemaingForLevel == nil then
                    trainingsRemaingForLevel = settings:get("trainingLimit")
                end
                if level > lastKnownLevel then
                    trainingsRemaingForLevel  = settings:get("trainingLimit")
                    lastKnownLevel = level
                end
                if trainingsRemaingForLevel == 0 then
                    I.UI.setMode("Dialogue", data.arg)
                    ui.showMessage(settings:get("noMoreTraining") or "You've already trained as many times as you can for this level.")
                else
                    async:newUnsavableSimulationTimer(delay1, function()
                        local soundPlaying = ambient.isSoundPlaying("skillraise")
                        if soundPlaying then
                            pendingTraining = true
                            async:newUnsavableSimulationTimer(delay2, function()
                              checkForTraining()
                            end)
                        end
                    end)
                end
            elseif data.newMode == "Dialogue" and pendingTraining then
                checkForTraining()
            end
        end,

    },
}
