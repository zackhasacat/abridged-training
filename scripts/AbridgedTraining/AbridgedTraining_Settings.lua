
local I = require("openmw.interfaces")
local key = "SettingsAT"

I.Settings.registerPage {
     key = key,
     l10n = key,
     name = "Abridged Training",
}
I.Settings.registerGroup {
     key = key,
     page = key,
     l10n = key,
     name = "Abridged Training",
     permanentStorage = true,
     settings = {

          {
               key = "noMoreTraining",
               renderer = "textLine",
               name = "Line to display when you cannot train any longer",
               default = "You've already trained as many times as you can for this level."
          },
          {
               key = "trainingLimit",
               renderer = "number",
               name = "How many times you can use training per level",
               default = 5
          },
     },
    }